using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ControMetalIzquierda : MonoBehaviour
{

   

    public Transform objDerecha;
    public Transform objIzquierda;

    private bool moverDerecha;
    private bool moverIzquierda;


    public float speed = 10f; 
    

    void Start()
    {
        moverIzquierda = true;
        moverDerecha = false;
           
    }

    void Update()
    {

        if(moverDerecha)
        {
            transform.position = Vector3.MoveTowards(transform.position, objDerecha.position, speed * Time.deltaTime);

            if(transform.position == objDerecha.position ) 
            {
                moverDerecha = false;
                moverIzquierda = true;
            }
        }

        if(moverIzquierda) 
        {
            transform.position = Vector3.MoveTowards(transform.position, objIzquierda.position, speed * Time.deltaTime);

            if(transform.position == objIzquierda.position ) 
            {
                moverIzquierda = false;
                moverDerecha =true;
            }

        }

    }


}

