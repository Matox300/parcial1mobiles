using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControlBotonSaltar : MonoBehaviour
{
    private Rigidbody rbEsfera;
    public GameObject esfera;


    private float magnitudSalto = 30f;

    private bool puedeSaltarGlobal;
    private bool puedeSaltarDobleGlobal;
    private bool tocoPisoGlobal;


    private int puedeSaltarDobleFlag = 0;

    void Start()
    {
        rbEsfera = esfera.GetComponent<Rigidbody>();
        puedeSaltarDobleFlag = 0;
    }

    public void OnPointDown()
    {
        tocoPisoGlobal = true;
        ControlJugador.tocoPiso = tocoPisoGlobal;
        puedeSaltarGlobal = ControlJugador.puedeSaltar;
        if(puedeSaltarGlobal) 
        {
            rbEsfera.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            puedeSaltarGlobal = false;
            ControlJugador.puedeSaltar = false;
        }

        puedeSaltarDobleGlobal = ControlJugador.puedeSaltarDoble;

        if(puedeSaltarDobleGlobal && puedeSaltarDobleFlag <= 1) 
        {
            rbEsfera.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            puedeSaltarDobleFlag++;
        }else if(puedeSaltarDobleFlag == 2)
        {
            puedeSaltarDobleFlag = 0;
            puedeSaltarDobleGlobal = false;
            ControlJugador.puedeSaltarDoble = false;
        }
    }
}
