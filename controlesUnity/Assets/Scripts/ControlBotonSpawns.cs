using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlBotonSpawns : MonoBehaviour
{
    private  bool spawnGlobalUno = false;
    private  bool spawnGlobalDos = false;
    private  bool spawnGlobalTres = false;
    private bool jugadorMuereGlobal = false;
    private bool jugadorGanaGlobal = false;
    public   GameObject jugador;

    private void Start()
    {
         spawnGlobalUno = false;
         spawnGlobalDos = false;
         spawnGlobalTres = false;
         jugadorMuereGlobal = false;
         jugadorGanaGlobal = false;
    }


    private void Update()
    {
        jugadorMuereGlobal = ControlJugador.jugadorMuere;
        if(jugadorMuereGlobal)
        {
            RealizarCheckpoint();
            ControlJugador.jugadorMuere = false;
            jugadorMuereGlobal = false;
        }
    }


    void RealizarCheckpoint()
    {
        spawnGlobalUno = ControlCheckpoint.spawnUno;
        spawnGlobalDos = ControlCheckpointDos.spawnDos;
        spawnGlobalTres = ControlCheckpointTres.spawnTres;
        jugadorGanaGlobal = ControlJugador.jugadorGano;
        if (spawnGlobalTres)
        {
            jugador.transform.position = new Vector3(1.4f, 10f, 1250f);
        }
        else if (spawnGlobalDos)
        {
            jugador.transform.position = new Vector3(1.4f, 10f, 766f);
        }
        else if (spawnGlobalUno)
        {
            jugador.transform.position = new Vector3(1.4f, 10f, 472f);
        }
        else
        {
            SceneManager.LoadScene("SampleScene");
        }

        if (jugadorGanaGlobal)
        {
            SceneManager.LoadScene("SampleScene");

        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            RealizarCheckpoint();
        }
                            
    }

}
