using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlBotonSpawnsNivelDos : MonoBehaviour
{
    private  bool spawnGlobalCuatro = false;


    private bool jugadorMuereGlobal = false;
    private bool jugadorGanaGlobal = false;
    public   GameObject jugador;

    private void Start()
    {
         spawnGlobalCuatro = false;
         jugadorMuereGlobal = false;
         jugadorGanaGlobal = false;
    }


    private void Update()
    {
        jugadorMuereGlobal = ControlJugador.jugadorMuere;
        if(jugadorMuereGlobal)
        {
            RealizarCheckpoint();
            ControlJugador.jugadorMuere = false;
            jugadorMuereGlobal = false;
        }
    }


    void RealizarCheckpoint()
    {
        spawnGlobalCuatro = ControlCheckpointCuatro.spawnCuatro;
        jugadorGanaGlobal = ControlJugador.jugadorGano;
  
        if (spawnGlobalCuatro)
        {
            jugador.transform.position = new Vector3(0.3f, 15f, 656f);
        }
        else
        {
            SceneManager.LoadScene("NivelDos");
        }

        if (jugadorGanaGlobal)
        {
            SceneManager.LoadScene("SampleScene");

        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            RealizarCheckpoint();
        }
                    
    }

}
