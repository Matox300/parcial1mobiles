using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlCheckpoint : MonoBehaviour
{

    public GameObject jugador;
    public GameObject textoCheckpoint;
    private int checkpointFlag;

    public static bool spawnUno = false;

    void Start()
    {
        spawnUno = false;
        checkpointFlag = 0;
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            spawnUno = true;
            if(checkpointFlag == 0)
            {
                textoCheckpoint.SetActive(true);
                StartCoroutine(MostrarTexto());
                checkpointFlag++;
            }
            
        }
    }

    public IEnumerator MostrarTexto()
    {
        yield return new WaitForSeconds(1f);

        textoCheckpoint.SetActive(false);
    }


}
