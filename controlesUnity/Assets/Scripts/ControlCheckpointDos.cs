using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ControlCheckpointDos : MonoBehaviour
{
    public GameObject jugador;
    public GameObject textoCheckpoint;
    private int checkpointFlag;


    public static bool spawnDos;

    void Start()
    {
        spawnDos = false;
        checkpointFlag = 0;

    }



    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            spawnDos = true;
            if (checkpointFlag == 0)
            {
                textoCheckpoint.SetActive(true);
                StartCoroutine(MostrarTexto());
                checkpointFlag++;
            }
        }
    }


    public IEnumerator MostrarTexto()
    {
        yield return new WaitForSeconds(1f);

        textoCheckpoint.SetActive(false);
    }

}
