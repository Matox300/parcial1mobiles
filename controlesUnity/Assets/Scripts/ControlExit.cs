using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlExit : MonoBehaviour
{
    // Start is called before the first frame update
    public void Exit()
    {
        if (Application.isPlaying)
        {
            Application.Quit();
        }
    }
}
