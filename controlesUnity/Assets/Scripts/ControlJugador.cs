using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ControlJugador : MonoBehaviour
{


    public GameObject particula;

    private float moveSpeed = 12f;
    Rigidbody rb;

    private float magnitudSalto = 30f;

    public GameObject textoVictoria;
    public GameObject textoTutorial; 
    public static bool puedeSaltar = true;

    public static bool tocoPiso = false;  

    public static bool puedeSaltarDoble = false;
    
    public static bool jugadorMuere = false;

    public static bool jugadorGano = false;


    private int puedeSaltarDobleFlag = 0;

    private void Awake()
    {
       Time.timeScale = 1.0f;
    }

    void Start()
    {        
        tocoPiso = true;
        puedeSaltar = true;
        Input.gyro.enabled = true;
        rb = GetComponent<Rigidbody>();
        puedeSaltarDoble = false;
        jugadorMuere = false;
        jugadorGano = false;
        puedeSaltarDobleFlag = 0;
    }

    void Update()
    {

        Quaternion deviceRotation = Input.gyro.attitude;

 
        Quaternion rotationFix = new Quaternion(deviceRotation.x, deviceRotation.y, -deviceRotation.z, -deviceRotation.w);
        transform.rotation = rotationFix;

        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);


        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Jump();
            }
        }


    }

    private void Jump()
    {
        tocoPiso = true;
        if (puedeSaltar)
        {
            textoTutorial.SetActive(false);
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            puedeSaltar = false;
            ControlJugador.puedeSaltar = false;
        }

        if (puedeSaltarDoble && puedeSaltarDobleFlag <= 1)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            puedeSaltarDobleFlag++;
        }
        else if (puedeSaltarDobleFlag == 2)
        {
            puedeSaltarDobleFlag = 0;
            puedeSaltarDoble = false;
            ControlJugador.puedeSaltarDoble = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("piso"))
        {            
            puedeSaltar = true;
            if(tocoPiso)
            {
                InstanciarParticulas();
                tocoPiso = false;                
            }                        
        }

        if (collision.gameObject.CompareTag("pisoDobleSalto") == true)
        {
            puedeSaltarDoble = true;
            if (tocoPiso)
            {
                InstanciarParticulas();
                tocoPiso = false;
            }
        }
        if(collision.gameObject.CompareTag("pinchoRojo")== true)
        {
            jugadorMuere = true;
            transform.Translate(Vector3.forward *0 *Time.deltaTime);
        }
        if (collision.gameObject.CompareTag("victoriaNivelUno") == true)
        {
            textoVictoria.SetActive(true);
            jugadorGano= true;
            ControlCheckpoint.spawnUno = false;
            ControlCheckpointDos.spawnDos = false;
            ControlCheckpointTres.spawnTres = false;
            StartCoroutine(PasarDeNivel());
        }
        if (collision.gameObject.CompareTag("victoria") == true)
        {
            gameObject.SetActive(false);
            textoVictoria.SetActive(true);
            jugadorGano = true;
            ControlCheckpoint.spawnUno = false;
            ControlCheckpointDos.spawnDos = false;
            ControlCheckpointTres.spawnTres = false;            
        }



    }


    public IEnumerator PasarDeNivel()
    {
        yield return new WaitForSeconds(2f);

        SceneManager.LoadScene("NivelDos");
    }

    private void InstanciarParticulas()
    {
        GameObject particulas = Instantiate(particula, transform.position, Quaternion.identity);
        Destroy(particulas, 1);
        
    }


}
