using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ControlPausa : MonoBehaviour
{

    [SerializeField] private GameObject menuPanel;


    public void Pausa()
    {
        gameObject.SetActive(false);
        Time.timeScale = 0f;
        menuPanel.SetActive(true);
    }

}

