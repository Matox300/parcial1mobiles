using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ControlPinchoMetalico : MonoBehaviour
{
 
    public Transform limitePincho;
    public Transform inicioPincho;

    private bool moverArriba;
    private bool moverAbajo;


    private float speed = 20f; 

    void Start()
    {
        moverArriba = true;
        moverAbajo = false;       
    }

    void Update()
    {

        if(moverArriba)
        {
            transform.position = Vector3.MoveTowards(transform.position, limitePincho.position, speed * Time.deltaTime);

            if(transform.position == limitePincho.position ) 
            {
                moverArriba = false;
                moverAbajo = true;
            }
        }

        if(moverAbajo) 
        {
            transform.position = Vector3.MoveTowards(transform.position, inicioPincho.position, speed * Time.deltaTime);

            if(transform.position == inicioPincho.position ) 
            {
                moverAbajo = false;
                moverArriba =true;
            }

        }

    }


}

