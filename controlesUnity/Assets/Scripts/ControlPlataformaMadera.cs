using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlataformaMadera : MonoBehaviour
{

    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        
    }

    IEnumerator SoltarBloque()
    {

        yield return new WaitForSeconds(2f);

        rb.isKinematic = false;

        Destroy(gameObject, 4f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(SoltarBloque());                        
        }
    }

}
