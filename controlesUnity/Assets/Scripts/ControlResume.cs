using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlResume : MonoBehaviour
{
    [SerializeField] private GameObject menuPanel;
    [SerializeField] private GameObject botonPausa;



    public void Continuar()
    {
        Time.timeScale = 1.0f;
        botonPausa.SetActive(true);
        menuPanel.SetActive(false);

    }
}
