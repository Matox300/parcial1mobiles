using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPart : MonoBehaviour
{
    public Transform player; 
    private float activationDistance = 300f; 
    private Renderer renderer;
    private Collider collider;

    void Start()
    {        
        renderer = GetComponent<Renderer>();
        collider = GetComponent<Collider>();
    }

    void Update()
    {
        if (player == null) return;


        float distance = Vector3.Distance(player.position, transform.position);

        if (distance > activationDistance)
        {
            if (renderer.enabled)
            {
                renderer.enabled = false;
                collider.enabled = false;
            }
        }
        else
        {
            if (!renderer.enabled)
            {
                renderer.enabled = true;
                collider.enabled = true;
            }
        }
    }
}